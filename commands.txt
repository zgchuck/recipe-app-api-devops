docker-compose -f deploy/docker-compose.yml run --rm terraform workspace list
docker-compose -f deploy/docker-compose.yml run --rm terraform workspace dev

docker-compose -f deploy/docker-compose.yml run --rm terraform fmt
docker-compose -f deploy/docker-compose.yml run --rm terraform validate
docker-compose -f deploy/docker-compose.yml run --rm terraform plan
docker-compose -f deploy/docker-compose.yml run --rm terraform apply

just to run pipeline
